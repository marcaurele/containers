# Audiotools

Image containing a list of audiotools to perform transformation.

```console
docker run --rm -v `pwd`:/data docker.io/marcaurele/audiotools bash
```

## Tools

- `AAXtoMP3` <https://github.com/KrumpetPirate/AAXtoMP3>
- `audible`
- `bchunk`
- `cuetoolss`
- `jq`
- `mediainfo`
- `mp4v2` <https://github.com/enzo1982/mp4v2>
- `sox`

## Extra

There is an extra tool not included, but already available as a docker image <https://github.com/sandreas/m4b-tool>:

```console
# pull the image
docker pull sandreas/m4b-tool:latest

# create an alias for m4b-tool running docker
alias m4b-tool='docker run -it --rm -u $(id -u):$(id -g) -v "$(pwd)":/mnt sandreas/m4b-tool:latest'

# testing the command
m4b-tool --version
```
