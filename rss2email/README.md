# RSS2Email

RSS2Email is a python package providing a RSS feed reader to push new content into your mailbox. See project at <https://github.com/rss2email/rss2email>.

## Usage

```console
docker run --rm -v `pwd`:/data docker.io/marcaurele/rss2email
```
