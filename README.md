# Containers

Series of containers to isolate the execution of tools and lower the required packages on a system.
The base image is alpine to try to reduce the overall size. All images are build with the
same base image to use caching as much as possible when running those.

## List of containers

### Music

- [audiotools](./audiotools/) - `docker pull docker.io/marcaurele/audiotools`

### RSS feeds

- [rss2email](./rss2email/) - `docker pull docker.io/marcaurele/rss2email`

### Tools

- [makedeb](./makedeb/) - `docker pull docker.io/marcaurele/makedeb:debian-testing-latest`
