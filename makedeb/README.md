# Makedeb

`makedeb` packaging tool provided with different debian base image, especially the `testing` one. See <https://docs.makedeb.org/> for more information.
